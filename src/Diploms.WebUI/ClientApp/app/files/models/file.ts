export class File {
    public id: number = 0;
    public name: string = "";
    public size: string = "";
}
export class FileList {
    public files: File[] = [];
}